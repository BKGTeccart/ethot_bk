﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ethot_bk.Controllers
{
    public class MessageController : Controller
    {
        ethot_linqDataContext eth = new ethot_linqDataContext();

        public ActionResult Index()
        {
            var prof = from Educator in eth.Educators
                        select Educator;

            return View(prof);
        }

        public ActionResult Send_m(Message msg)
        {
            msg.id_student = (int?)Session["id_st"];
            eth.Messages.InsertOnSubmit(msg);

            try
            {
                eth.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                eth.SubmitChanges();
            }

            return View("messagesent");
        }
    }
}