﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ethot_bk.Controllers
{
    
    public class InscriptionController : Controller
    {

        ethot_linqDataContext eth = new ethot_linqDataContext();

        // GET: Inscription
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult VerifyInscription(Student std)
        {
            var user = eth.Students.Where(x => x.email == std.email).FirstOrDefault();

            if (user == null)
            {
                eth.Students.InsertOnSubmit(std);
                std.password_ = "1234";

                try
                {
                    eth.SubmitChanges();
                    sendMail(std.email.Trim(), "Bienvenu! Votre mot de passe temporaire est : " + std.password_);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    eth.SubmitChanges();
                }


                return View("");
            }
            else
            {
                return View("UserAlreadyexists");
            }

           
        }


        private bool sendMail(string email, string message)
        {
            try
            {
                //fichier de securite qui contient les informations confidentiels de la companie : ConfigurationManager
                string MailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();
                string MailPw = System.Configuration.ConfigurationManager.AppSettings["MailPw"].ToString();

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 100000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(MailSender, MailPw);

                MailMessage mailMessage = new MailMessage(MailSender, email, "L'inscription", message);
                // mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}