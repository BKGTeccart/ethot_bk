﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ethot_bk.Controllers
{
    public class InscriptionCourseController : Controller
    {
        ethot_linqDataContext eth = new ethot_linqDataContext();
        public ActionResult Index()
        {

            var cours = from Cours in eth.Cours
                        select Cours;

            return View(cours);
        }

        [HttpPost]
        public ActionResult inscriptioncourse(Inscription inscr)
        {
            inscr.id_student = (int)Session["id_st"];

            var insription = eth.Inscriptions.Where(x => x.id_cours== inscr.id_cours && x.id_student == inscr.id_student).FirstOrDefault();

            if(insription == null)
            {
                inscr.createdAt = DateTime.Now;
                eth.Inscriptions.InsertOnSubmit(inscr);

                try
                {
                    eth.SubmitChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    eth.SubmitChanges();
                }

                return View("inscriptionok");
            } else
            {
                return View("dejainscrit");
            }
            
        }
    }


}