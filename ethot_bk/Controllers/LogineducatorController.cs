﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ethot_bk.Controllers
{
    public class LogineducatorController : Controller
    {
        ethot_linqDataContext eth = new ethot_linqDataContext();

        // GET: Logineducator
        public ActionResult Index()
        {
            return View();
        }



        [HttpPost]
        public ActionResult VerifyLoginEduc(Educator edc)
        {

            if (edc.email != null && edc.password_edc != null)
            {

                var user = eth.Educators.Where(x => x.email == edc.email && x.password_edc == edc.password_edc);
                if (user != null)
                {
                    Session["email"] = edc.email;
                    Session["id_st"] = (from Educator in eth.Educators
                                        where Educator.email == edc.email && Educator.password_edc == edc.password_edc
                                        select Educator.id_educator).SingleOrDefault();

                    return RedirectToAction("Index", "AllCourses", new { area = "" });
                }
                else
                {

                    return View("../Login/Error");

                }

            }
            else
            {
                return View("../Login/Error");
            }

        }

        public ActionResult Deconexion()
        {
            Session.Clear();
            return RedirectToAction("Index", "Logineducator", new { area = "" });
        }
    }
}