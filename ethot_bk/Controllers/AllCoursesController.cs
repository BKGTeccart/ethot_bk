﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ethot_bk.Controllers
{
    public class AllCoursesController : Controller
    {
        ethot_linqDataContext eth = new ethot_linqDataContext();

        // GET: AllCourses
        public ActionResult Index()
        {
            var cours = from Cours in eth.Cours
                        select Cours;

            return View(cours);
        }

        public ActionResult Delete(int id_cours)
        {
            //delete from inscription first
            var DeleteID_ins = from d in eth.Inscriptions
                           where d.id_cours == id_cours
                           select d;

            eth.Inscriptions.DeleteAllOnSubmit(DeleteID_ins);
            eth.SubmitChanges();

            //check if records still exist
            var course = eth.Inscriptions.Where(x => x.id_cours == id_cours);
            if (course != null)
            {
                //return error
                return View("../Login/Error");
            }
            else
            {
                //return true
                var DeleteID = from d in eth.Cours
                               where d.id_cours == id_cours
                               select d;

                eth.Cours.DeleteAllOnSubmit(DeleteID);
                eth.SubmitChanges();

                var course_cr = eth.Cours.Where(x => x.id_cours == id_cours);
                if(course_cr != null)
                {
                    //Error
                    return View("../Login/Error");
                } else
                {
                    //Success
                    return View("../Login/Success");
                }


            }

        }

        public ActionResult AddCourse()
        {
            return RedirectToAction("Index", "AddCourse", new { area = "" });
        }
    }
}