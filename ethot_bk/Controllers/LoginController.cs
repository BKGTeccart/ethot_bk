﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;

namespace ethot_bk.Controllers
{
    public class LoginController : Controller
    {

        ethot_linqDataContext eth = new ethot_linqDataContext();

        [HttpGet]
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult VerifyLogin(Student std)
        {

            if (std.email != null && std.password_ != null)
            {

                var user = eth.Students.Where(x => x.email == std.email && x.password_ == std.password_).FirstOrDefault();
                if (user!=null)
                {
                    Session["id_st"] = (from Student in eth.Students
                                        where Student.email == std.email && Student.password_ == std.password_
                                        select Student.id_student).SingleOrDefault();

                    if (std.password_ == "1234")
                    {
                        //changer le mot de passe si c'est la premiere fois.
                        return View("Changepassword");

                    } else
                    {
                        Session["email"] = std.email;
                        return RedirectToAction("Index", "Cours", new { area = "" });
                    }
                    
                }
                else
                {
                    return View("Error");

                }

            }
            else
            {
                return View("Error");
            }

        }
        
        [HttpPost]
        public ActionResult Resetpassword(string password_, string password_2)
        {
            if (password_ == "1234" && password_2 !="")
            {
                int id_st = (int)Session["id_st"];
                (from p in eth.Students
                 where p.id_student == id_st
                 select p).ToList()
                                        .ForEach(x => x.password_ = password_2);

                eth.SubmitChanges();

                return View("PasswordDone");
            } else
            {
                return View("Error");
            }

        }


    }
}