﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace ethot_bk.Controllers
{
    public class CoursController : Controller
    {
        ethot_linqDataContext eth = new ethot_linqDataContext();

        // GET: Cours
        public ActionResult Index()
        {
            if (Session["id_st"] is int)
            {
                int id_student = (int)Session["id_st"];

                if (Session["id_st"] == null)
                {
                    return View("../Login/Error");
                }
                else
                {

                    /*var cours = from Cours in eth.Cours
                                join inscription in eth.Inscriptions on Cours.id_cours equals inscription.id_cours
                                join student in eth.Students on inscription.id_student equals student.id_student
                                where student.id_student == id_student
                                select Cours;

                    return View(cours);*/

                    Array array;

                    var global_model = (from Cours in eth.Cours
                                       join Materiel in eth.Materiels on Cours.id_cours equals Materiel.id_cours
                                       join inscription in eth.Inscriptions on Cours.id_cours equals inscription.id_cours
                                       join student in eth.Students on inscription.id_student equals student.id_student
                                       where student.id_student == id_student
                                       select new Models.Globalclass { cours_data = Cours, materiel_data=Materiel}).ToList<Models.Globalclass>();

                    array = global_model.ToArray();
                    ViewBag.Records = array;
                    return View();

                }
            }
            else
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            
           
        }

        public ActionResult Deconexion()
        {
            Session.Clear();
            return RedirectToAction("Index", "Login", new { area = "" });
        }
        public ActionResult sendmessage()
        {
            return RedirectToAction("Index", "Message", new { area = "" });
        }        
        
        public ActionResult inscription()
        {
           
            return RedirectToAction("Index", "InscriptionCourse", new { area = "" });
        }
    }
}