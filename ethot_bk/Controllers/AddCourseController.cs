﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ethot_bk.Controllers
{
    public class AddCourseController : Controller
    {
        ethot_linqDataContext eth = new ethot_linqDataContext();
        // GET: AddCourse
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add_c(Cour cr_)
        {
            eth.Cours.InsertOnSubmit(cr_);

            try
            {
                eth.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                eth.SubmitChanges();
            }

            return View("courseaddedsuccessfully");
        }

    }
}